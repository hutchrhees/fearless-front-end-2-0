import React from 'react';

class ConferenceForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            name: '',
            starts: '',
            ends: '',
            maxPresentations: '',
            maxAttendees: '',
            description: '',
            locations: []
        };
        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartChange = this.handleStartChange.bind(this);
        this.handleEndChange = this.handleEndChange.bind(this);
        this.handleMaxPresentationsChange = this.handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    async handleNameChange(event) {
        const value = event.target.value;
        this.setState({ name: value })
    }
    async handleStartChange(event) {
        const value = event.target.value;
        this.setState({ starts: value })
    }
    async handleEndChange(event) {
        const value = event.target.value;
        this.setState({ ends: value })
    }
    async handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({ maxPresentations: value })
    }
    async handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({ maxAttendees: value })
    }
    async handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({ description: value })
    }
    async handleLocationChange(event) {
        const value = event.target.value;
        this.setState({ location: value })
    }
    async handleSubmit(event) {
        event.preventDefault();
        const data = { ...this.state };
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            const newConference = await response.json();
            console.log(newConference);

            const cleared = {
                name: '',
                starts: '',
                ends: '',
                maxPresentations: '',
                maxAttendees: '',
                description: '',
                location: '',
            };
            this.setState(cleared);
        }
    }

    async componentDidMount() {

        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            this.setState({ locations: data.locations });
        }
    }
    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a new Conference</h1>
                        <form onSubmit={this.handleSubmit} id="create-conference-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name} />
                                <label htmlFor="name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleStartChange} placeholder="starts" required type="date" name="starts" id="starts" className="form-control" value={this.state.starts} />
                                <label htmlFor="starts">Starts</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEndChange} placeholder="ends" required type="date" name="ends" id="ends" className="form-control" value={this.state.ends} />
                                <label htmlFor="ends">Ends</label>
                            </div>
                            <div className="mb-3">
                                <label htmlFor="description" className="form-label">Description</label>
                                <textarea onChange={this.handleDescriptionChange} value={this.state.description} placeholder="Description" className="form-control" name="description" id="description" rows="3" ></textarea>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleMaxPresentationsChange} placeholder="max presentations" required type="number" name="max_presentations"
                                    id="max_presentations" className="form-control" value={this.state.maxPresentations} />
                                <label htmlFor="max_presentations">Maximum Presentations</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleMaxAttendeesChange} placeholder="max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={this.state.maxAttendees} />
                                <label htmlFor="max_attendees">Maximum Attendees</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={this.handleLocationChange} required name="location" id="location" className="form-select" value={this.state.location} >
                                    <option value="">Choose a Location</option>
                                    {this.state.locations.map(location => {
                                        return (
                                            <option key={location.id} value={location.id}>
                                                {location.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }

}

export default ConferenceForm;
